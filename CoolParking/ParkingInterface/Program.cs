﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using CoolParking.BL.Services;
using System;
using System.IO;
using System.Reflection;
using System.Linq;

namespace ParkingInterface
{
    class Program
    {
        static void Main(string[] args)
        {
            bool endApp = false;
            Console.WriteLine("Cool parking !!!!!\n");
            
            var withdrawTimer = new TimeService(Settings.PaymentPeriod);
            var logTimer = new TimeService(Settings.LogWritingPeriod); 
            var logService = new LogService($@"{Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)}\Transactions.log");
            ParkingService parkingService = new ParkingService(withdrawTimer,logTimer,logService);
            while (!endApp)
            {
                Console.WriteLine("\n\n\tMenu:\n");
                Console.WriteLine("1.Press 1 to display current balance of the parking.");
                Console.WriteLine("2.Press 2 to display amount earned since last writing to log.");
                Console.WriteLine("3.Press 3 to display number of free parking places.");
                Console.WriteLine("4.Press 4 to display all transactions since last writing to log.");
                Console.WriteLine("5.Press 5 to display all transactions that are in log file.");
                Console.WriteLine("6.Press 6 to display all Vehicle that are on parking.");
                Console.WriteLine("7.Press 7 to add Vehicle on parking.");
                Console.WriteLine("8.Press 8 to remove Vehicle on parking.");
                Console.WriteLine("9.Press 9 to top up Vehicle balance.");
                Console.WriteLine("Press 'e' and Enter to close the app.");
                
                switch (Console.ReadLine())
                {
                    case "1":
                        Console.WriteLine($"Current balance = {parkingService.GetBalance()}");
                        break;
                    case "2":
                        var curTransactions = parkingService.GetLastParkingTransactions();
                        Console.WriteLine($"Amount since last writing to log = {curTransactions.Sum(tr => tr.Sum)}");
                        break;
                    case "3":
                        Console.WriteLine($"Parking has {parkingService.GetFreePlaces()} free places.");
                        break;
                    case "4":
                        Console.WriteLine("List of all transactions since last writing to log:");
                        var currentTransactions = parkingService.GetLastParkingTransactions();
                        foreach(var transaction in currentTransactions)
                        {
                            Console.WriteLine(transaction.GetTransactionInfo());
                        }    
                        break;
                    case "5":
                        Console.WriteLine("List of all transactions that are in log file:");
                        try
                        {
                            Console.WriteLine(parkingService.ReadFromLog());
                        }
                        catch(InvalidOperationException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case "6":
                        Console.WriteLine("List of all Vehicle that are on parking:");
                        var vehicles = parkingService.GetVehicles();
                        foreach (var vehicle in vehicles)
                        {
                            Console.WriteLine(vehicle.ToString());
                        }
                        break;
                    case "7":
                        Console.WriteLine("Process of adding vehicle:");
                        try
                        {
                            Console.WriteLine("Input vehicle id(Format: AA-1111-AA, where A is any uppercase letter, and 1 is any digit):");
                            string id = Console.ReadLine();
                            Console.WriteLine("Input vehicle type. Choose one from : PassengerCar, Truck, Bus, Motorcycle:");
                            string typeInput = Console.ReadLine();
                            VehicleType type;
                            while (!Enum.TryParse(typeInput, out type))
                            {
                                Console.Write("This is not valid input. Please try again: ");
                                typeInput = Console.ReadLine();
                            }
                            Console.WriteLine("Input vehicle initial balance (must be >= 0)");
                            string balanceInput = Console.ReadLine();
                            decimal balance = 0;
                            while (!decimal.TryParse(balanceInput, out balance))
                            {
                                Console.Write("This is not valid input. Please try again: ");
                                balanceInput = Console.ReadLine();
                            }
                            var newVehicle = new Vehicle(id, type, balance);
                            parkingService.AddVehicle(newVehicle);
                        }
                        catch(ArgumentException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        catch(InvalidOperationException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        catch(Exception e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case "8":
                        Console.WriteLine("Process of removing vehicle:");
                        try
                        {
                            Console.WriteLine("Input vehicle id(Format: AA-1111-AA, where A is any uppercase letter, and 1 is any digit):");
                            string id = Console.ReadLine();
                            parkingService.RemoveVehicle(id);
                            Console.WriteLine($"Vehicle with id: {id} was removed from parking");
                        }
                        catch (ArgumentException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        catch(InvalidOperationException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case "9":
                        Console.WriteLine("Process of topping up vehicle's balance:");
                        try
                        {
                            Console.WriteLine("Input vehicle id(Format: AA-1111-AA, where A is any uppercase letter, and 1 is any digit):");
                            string id = Console.ReadLine();
                            Console.WriteLine("Input amount you want to add (must be >= 0");
                            string balanceInput = Console.ReadLine();
                            decimal balance = 0;
                            while (!decimal.TryParse(balanceInput, out balance))
                            {
                                Console.Write("This is not valid input. Please try again: ");
                                balanceInput = Console.ReadLine();
                            }
                            parkingService.TopUpVehicle(id,balance);
                            Console.WriteLine($"Vehicle's balance with id: {id} was successfully topped up");
                        }
                        catch (ArgumentException e)
                        {
                            Console.WriteLine(e.Message);
                        }
                        break;
                    case "e":
                        endApp = true;
                        break;
                    default:
                        Console.Write("This is not valid input. Please enter value in range [1;9].\n\n ");
                        break;
                }
                
            }
        }
    }
}
