﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.
using CoolParking.BL.Interfaces;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimeService: ITimerService
    {
        private Timer _aTimer;

        public event ElapsedEventHandler Elapsed;
        public double Interval { get; set; }
        public TimeService()
        {
            Elapsed = null;
        }
        public TimeService(double interval)
        {
            Interval = interval;
            Elapsed = null;
            _aTimer = new Timer(Interval * 1000);
            _aTimer.Elapsed += FireElapsedEvent;
            _aTimer.AutoReset = true;
        }
        public void FireElapsedEvent(object sender, ElapsedEventArgs e)
        {
            Elapsed?.Invoke(this, null);
        }
        public void Start()
        {
            _aTimer.Start();
        }
        public void Stop()
        {
            _aTimer.Stop();
        }
        public void Dispose()
        {
            _aTimer.Dispose();
            Elapsed = null;
        }
    }
}