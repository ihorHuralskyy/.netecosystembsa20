﻿// TODO: implement struct TransactionInfo.
//       Necessarily implement the Sum property (decimal) - is used in tests.
//       Other implementation details are up to you, they just have to meet the requirements of the homework.
using System;

namespace CoolParking.BL.Models
{
    public struct TransactionInfo
    {
        public DateTime TransactionTime;
        public string VehicleId;
        public decimal amount;
        public decimal Sum { get; set; }
        public TransactionInfo(DateTime traTime,string id,decimal am)
        {
            TransactionTime = traTime;
            VehicleId = id;
            amount = am;
            Sum = amount;
        }
        public string GetTransactionInfo()
        {
            return $"Transaction at {TransactionTime}. From vehicle with id: {VehicleId}. Amount = {amount}.";
        }
    }
}