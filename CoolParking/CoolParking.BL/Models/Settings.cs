﻿// TODO: implement class Settings.+
//       Implementation details are up to you, they just have to meet the requirements of the home task.+

using System;


namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal InitialParkingBalance = 0m;
        public static int ParkingCapacity = 10;
        public static int PaymentPeriod = 5;
        public static int LogWritingPeriod = 60;
        public static decimal FineRatio = 2.5m;
        public static decimal VehicleTariff(VehicleType vehicleType)
        {
            switch(vehicleType)
            {
                case VehicleType.PassengerCar:
                    return 2m;
                   
                case VehicleType.Truck:
                    return 5m;
                   
                case VehicleType.Bus:
                    return 3.5m;
                    
                case VehicleType.Motorcycle:
                    return 1m;
                    
                default:
                    throw new Exception("VehicleType must be:PassengerCar, Truck, Bus, Motorcycle.");
                    
            }
        }
    }
}